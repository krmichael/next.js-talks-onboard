import axios from "axios";

export default function Users({ users }) {
  return (
    <div className="container">
      <h1>Next.js Talks - Onboard</h1>

      {users.map((u) => (
        <div key={u.id} className="card">
          <h3>{u.name}</h3>
          <span>{u.email}</span>
        </div>
      ))}
      <style jsx>{`
        .container {
          padding: 20px;
          max-width: 1200px;
          margin: 0px auto;
        }
        .card {
          padding: 20px;
          border: 0.5px solid #ccc;
          border-radius: 4px;
          margin-bottom: 20px;
        }
      `}</style>
    </div>
  );
}

export async function getServerSideProps() {
  const response = await axios.get(
    "https://jsonplaceholder.typicode.com/users"
  );
  const users = await response.data;

  return {
    props: {
      users: users.map(({ id, name, email }) => ({
        id,
        name,
        email,
      })),
    },
  };
}
